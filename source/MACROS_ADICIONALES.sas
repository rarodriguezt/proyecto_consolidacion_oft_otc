

%MACRO USOP(VISTA_ANO, PREDIO_ANO,PREDIO_USO_ANO,CODIGO_USOP_ANO);/*VISTA_ANO=BASE CON VISTA CALIFICACION DEL A�O CORRESPONDIETE,
 
/* EXTRACCI�N DEL USO DE LA BASE VISTA_CALIFICACI�N */
PROC SQL;
	CREATE TABLE USOS_VISTA AS
 	SELECT CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, 
		  CODIGO_CONSTRUCCION, CODIGO_RESTO, VETUSTEZ,
		  UNIDAD_CALIFICADA, CODIGO_USO, AREA_USO, CLASE_CONSTRUCCION, PUNTAJE
     FROM &VISTA_ANO;
QUIT; 

/* SE CREA UNA BASE CON LA SUMA DE AREA DE USO POR PREDIO-USO */
PROC SQL;
	CREATE TABLE PREDIOUSO AS SELECT *, ROUND(SUM(AREA_USO),0.01) AS SUM_USO
	FROM USOS_VISTA
	GROUP BY CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, 
		    CODIGO_CONSTRUCCION, CODIGO_RESTO, CODIGO_USO;
QUIT;
 
/* SE CREA TABLA CON LA MODA DEL USO A NIVEL DE PREDIOS 

  (en caso de empates se selecciona  en orden 
   CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO CODIGO_CONSTRUCCION CODIGO_RESTO
   SUM_USO, UNIDAD_CALIFICADA, DESCENDING CLASE_CONSTRUCCION,  DESCENDING PUNTAJE, CODIGO_USO)
*/
PROC SQL;
     CREATE TABLE PREDIOSU AS
     SELECT DISTINCT CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, 
				 CODIGO_CONSTRUCCION, CODIGO_RESTO, CODIGO_USO, SUM_USO, UNIDAD_CALIFICADA, CLASE_CONSTRUCCION, PUNTAJE,
				 MAX(SUM_USO) AS MAX_PRED_USO, VETUSTEZ
     FROM PREDIOUSO 
     GROUP BY CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, 
		    CODIGO_CONSTRUCCION, CODIGO_RESTO 
     HAVING SUM_USO = MAX_PRED_USO;
QUIT;


PROC SORT DATA = PREDIOSU OUT = PREDIOSU1;
by CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO CODIGO_CONSTRUCCION CODIGO_RESTO
   SUM_USO UNIDAD_CALIFICADA DESCENDING CLASE_CONSTRUCCION  DESCENDING PUNTAJE CODIGO_USO;RUN;


/*******************************************************/
 

/*SE ELIMINAN DUPLICADOS POR PREDIO*/
PROC SORT DATA = PREDIOSU1 OUT = PREDIOS_UP NODUPKEY dupout=PREDIOSDUPLI1;
     BY CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO CODIGO_CONSTRUCCION CODIGO_RESTO;
RUN; 
 
/* PEGADO DEL USO PREDOMINANTE A LA BASE DE PREDIOS */
PROC SQL;
	CREATE TABLE &PREDIO_USO_ANO AS 
	SELECT A.*, B.CODIGO_USO AS &CODIGO_USOP_ANO, 
	B.PUNTAJE, B.VETUSTEZ
	FROM &PREDIO_ANO AS A LEFT JOIN PREDIOS_UP AS B 
	ON A.CODIGO_BARRIO=B.CODIGO_BARRIO AND A.CODIGO_MANZANA=B.CODIGO_MANZANA AND
	   A.CODIGO_PREDIO=B.CODIGO_PREDIO AND A.CODIGO_CONSTRUCCION = B.CODIGO_CONSTRUCCION AND 
	   A.CODIGO_RESTO=B.CODIGO_RESTO;
QUIT;
 
PROC SQL; DROP TABLE USOS_VISTA; QUIT;
PROC SQL; DROP TABLE PREDIOUSO; QUIT;
PROC SQL; DROP TABLE PrediosU; QUIT;
/*PROC SQL; DROP TABLE Predios_UP; QUIT;*/
 
%MEND USOP;


%MACRO PROCESS_LOTES(VIG);
	DATA LOTES_20&VIG.;
	SET INFO.LOTES_20&VIG.;
	VIGENCIA = 20&VIG.;
	IF LENGTH(BARMANPRE) = 10;
	KEEP VIGENCIA BARMANPRE CODIGO_BARRIO NOMBRE_BARRIO CODIGO_LOCALIDAD NOMBRE_LOCALIDAD CODIGO_UPZ NOMBRE_UPZ LONGITUD LATITUD;
	RUN;

	DATA LOTES_20&VIG.;
	SET LOTES_20&VIG.;
	RENAME LONGITUD = LONGITUD_SIIC;
	RENAME LATITUD = LATITUD_SIIC;
	RUN;

	PROC SORT DATA=LOTES_20&VIG. NODUPKEY; BY BARMANPRE VIGENCIA; RUN;

	PROC APPEND BASE=LOTES DATA=LOTES_20&VIG. FORCE;RUN;

	PROC SORT DATA=LOTES NODUPKEY; BY BARMANPRE VIGENCIA; RUN;
%MEND PROCESS_LOTES;

%MACRO PROCESS_PREDIO(VIG);

DATA PREDIO_FINAL_&VIG.;
SET COMUN.PREDIO_FINAL_20&VIG.;
BARMANPRE = CAT(CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO);
VIGENCIA = 20&VIG.;
KEEP CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO BARMANPRE CLASE_PREDIO DIRECCION_REAL VIGENCIA;
RUN;

PROC SORT DATA=PREDIO_FINAL_&VIG. NODUPKEY; BY CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO BARMANPRE; RUN;

PROC APPEND BASE=PREDIO_FINAL DATA=PREDIO_FINAL_&VIG. FORCE;RUN;

PROC SORT DATA=PREDIO_FINAL NODUPKEY; BY CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO BARMANPRE VIGENCIA; RUN;

%MEND PROCESS_PREDIO;


%MACRO PROCESS_ZHF(VIG);

%if &VIG. = 21 %THEN %DO;
DATA RESOLUCION_PREDIO_DIG_&vig.;
SET ODBCLIB.RESOLUCION_PREDIO_DIG_2022;
IF VIGENCIA_RESOLUCION = 2022;
BARMANPRE = CAT(CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO);
RENAME CODIGO_ZONA_FISICA = ZHF;
RUN;

DATA RESOLUCION_PREDIO_DIG_&vig.;
SET RESOLUCION_PREDIO_DIG_&vig.;
VIGENCIA = 2021;
RUN;
%END;
%ELSE %DO;
DATA RESOLUCION_PREDIO_DIG_&vig.;
SET ODBCLIB.RESOLUCION_PREDIO_DIG_20&VIG.;
IF VIGENCIA_RESOLUCION = 20&VIG.;
BARMANPRE = CAT(CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO);
RENAME CODIGO_ZONA_FISICA = ZHF VIGENCIA_RESOLUCION = VIGENCIA;
RUN;
%END;

DATA RESOLUCION_PREDIO_DIG_&vig.;
SET RESOLUCION_PREDIO_DIG_&vig.;
KEEP VIGENCIA BARMANPRE ZHF;
RUN;

PROC SORT DATA=RESOLUCION_PREDIO_DIG_&vig. NODUPKEY; BY BARMANPRE VIGENCIA; RUN;

proc append base=RESOLUCION_PREDIO_DIG DATA=RESOLUCION_PREDIO_DIG_&VIG. force;run;

PROC SORT DATA=RESOLUCION_PREDIO_DIG NODUPKEY; BY BARMANPRE VIGENCIA; RUN;

%MEND;


%MACRO GET_INDEX(VALUE);

	DATA DF;
	DO K = 1 TO ceil(log10(&VALUE. + 1)) BY 1;
	DIFF = ABS(SUBSTR(COMPRESS(put(&VALUE., 20.)), K, 1) - SUBSTR(COMPRESS(put(&VALUE., 20.)), K - 1, 1));
	IF DIFF > 0 THEN MARCA = 1; ELSE MARCA = 0;
		OUTPUT;
	END;
	RUN;

	DATA DF2;
	SET DF;
	retain TOTAL;
	TOTAL = sum(TOTAL, MARCA);
	LONG = ceil(log10(&VALUE. + 1)) - 1;
	PRECIO = &VALUE.;
	INDICE = TOTAL/LONG;
	KEEP PRECIO TOTAL LONG INDICE;
	RUN;

	data df3;
	set df2  end=last_obs;
	if last_obs then output;
	run;

	PROC APPEND BASE=BASE_FINAL DATA=DF3 FORCE; RUN;

%MEND GET_INDEX;


%macro MASSIVE_INDEX(DATATABLE);

%nrow(&DATATABLE., n);

%if %sysfunc(exist(WORK.FINAL_OUTPUT)) %then %do;
 	proc delete data=WORK.FINAL_OUTPUT; run;
 %end;

DATA &DATATABLE.;
SET &DATATABLE.;
ID = _n_;
run;

%do i=1 %to &n;
	proc sql noprint;
	  select PRECIO
	    into :VALUES separated by ' '
	    from &DATATABLE.
	where ID = &i.;
	quit;

	%GET_INDEX(&VALUES.);
/*	%PUT &I.;*/	
%end;

%mend MASSIVE_INDEX;



%MACRO CONSULTA_INFO_VIG(VIG);

	PROC SQL;
	CREATE TABLE INFO_&VIG. AS SELECT DISTINCT 
	CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, CODIGO_RESTO, CODIGO_CONSTRUCCION,
	CAT(CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, CODIGO_RESTO, CODIGO_CONSTRUCCION) AS CODIGO_SECTOR,
	DIRECCION_REAL,
	AREA_CONSTRUIDA AS AREA_CONSTRUIDA_SIIC, CODIGO_ESTRATO AS ESTRATO_SIIC, AREA_TERRENO AS AREA_TERRENO_SIIC,
	CODIGO_DESTINO AS CODIGO_DESTINO_SIIC, 
	20&VIG. AS VIGENCIA, "T" AS MARCA
	FROM COMUN.PREDIO_FINAL_20&VIG.
	WHERE CAT(CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, CODIGO_RESTO, CODIGO_CONSTRUCCION) IN 
	(SELECT DISTINCT CODIGO_SECTOR
	FROM BASE_OFT_CONSOLIDADA_7
	WHERE LENGTH(CODIGO_SECTOR) = 18);
	QUIT;

	data INFO_&VIG.;
	set INFO_&VIG.;
	NUMERO_PISO_SIIC = SUBSTR(CODIGO_RESTO,1,2)*1;
	RUN;

	PROC SQL;
	CREATE TABLE INFO_&VIG._NPH AS SELECT DISTINCT 
	CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, CODIGO_RESTO, CODIGO_CONSTRUCCION,
	CAT(CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, CODIGO_RESTO, CODIGO_CONSTRUCCION) AS CODIGO_SECTOR,
	DIRECCION_REAL,
	AREA_CONSTRUIDA AS AREA_CONSTRUIDA_SIIC, CODIGO_ESTRATO AS ESTRATO_SIIC, AREA_TERRENO AS AREA_TERRENO_SIIC,
	CODIGO_DESTINO AS CODIGO_DESTINO_SIIC, 
	20&VIG. AS VIGENCIA, "N" AS MARCA
	FROM COMUN.PREDIO_FINAL_20&VIG.
	WHERE CAT(CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO) IN 
	(SELECT DISTINCT CODIGO_SECTOR
	FROM BASE_OFT_CONSOLIDADA_7
	WHERE LENGTH(CODIGO_SECTOR) = 10 AND CLASE_PREDIO_SIIC = "N");
	QUIT;

	data INFO_&VIG._NPH;
	set INFO_&VIG._NPH;
	NUMERO_PISO_SIIC = SUBSTR(CODIGO_RESTO,1,2)*1;
	RUN;

	PROC SORT DATA=INFO_&VIG._NPH NODUPKEY; BY CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO;RUN;

	DATA INFO_&VIG.;
	SET INFO_&VIG. INFO_&VIG._NPH;
	RUN;

	%USOP(ODBCLIB.VISTA_CALIFICACION_20&VIG., INFO_&VIG., INFO_&VIG._2, CODIGO_USOP);

	DATA INFO_&VIG._3;
	SET INFO_&VIG._2;
	DROP VETUSTEZ;
	RUN;

	PROC SQL;
	CREATE TABLE INFO_&VIG._4 AS SELECT A.*, B.VETUSTEZ
	FROM INFO_&VIG._3 AS A 
	LEFT JOIN (SELECT CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, CODIGO_CONSTRUCCION, CODIGO_RESTO, MIN(VETUSTEZ) AS VETUSTEZ FROM ODBCLIB.VISTA_CALIFICACION
	WHERE COMPRESS(CAT(CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, CODIGO_CONSTRUCCION, CODIGO_RESTO)) IN
	(SELECT DISTINCT COMPRESS(CAT(CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, CODIGO_CONSTRUCCION, CODIGO_RESTO)) FROM INFO_&VIG.)
	GROUP BY CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, CODIGO_CONSTRUCCION, CODIGO_RESTO) AS B
	ON A.CODIGO_BARRIO=B.CODIGO_BARRIO AND
	A.CODIGO_MANZANA=B.CODIGO_MANZANA AND
	A.CODIGO_PREDIO=B.CODIGO_PREDIO AND 
	A.CODIGO_CONSTRUCCION=B.CODIGO_CONSTRUCCION AND 
	A.CODIGO_RESTO=B.CODIGO_RESTO; 
	QUIT;

	DATA INFO_&VIG._4;
	SET INFO_&VIG._4;
	IF MARCA = "N" THEN CODIGO_SECTOR = SUBSTR(COMPRESS(CODIGO_SECTOR), 1, 10);
	RUN;

	PROC SORT DATA=INFO_&VIG._4 NODUPKEY; BY CODIGO_SECTOR VIGENCIA; RUN;
	PROC APPEND BASE=INFO_SIIC DATA=INFO_&VIG._4 FORCE;RUN;
	PROC SORT DATA=INFO_SIIC NODUPKEY; BY CODIGO_SECTOR VIGENCIA; RUN;
%MEND;
