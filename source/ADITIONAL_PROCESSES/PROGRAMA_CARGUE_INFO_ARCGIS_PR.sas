/*proc import datafile = '\\prowin04\Prog_COMUN\ESTADISTICA\input\z.solicitud_arcgis\output\2022\03\2203_PR_ARCGIS.csv'*/
/* out = BASE_GEOREF*/
/* dbms = csv*/
/* replace;*/
/* delimiter = ';';*/
/*run;*/

proc import datafile = '\\prowin04\Prog_COMUN\ESTADISTICA\input\z.solicitud_arcgis\output\2022\04\2204_PR_ARCGIS.txt'
 out = BASE_GEOREF
 dbms = dlm
 replace;
 delimiter = ';';
run;


DATA BASE_GEOREF_2;
SET BASE_GEOREF;
LOTSECT_ID_2 = PUT(LOTSECT_ID, z6.); 
LOTMANZ_ID_2 = PUT(LOTMANZ_ID, z10.);
LOTLOTE_ID_2 = PUT(LOTLOTE_ID, z12.);
BARMANPRE_2 = PUT(BARMANPRE, z10.);
IDENTIFICADOR = CODIGO;
DROP CODIGO BARMANPRE LOTSECT_ID LOTMANZ_ID LOTLOTE_ID;
RUN;

LIBNAME PR_CON "\\prowin04\Prog_COMUN\ESTADISTICA\input\PR";

DATA PR_CON.geocodificador_pr_22_04_arcgis;
SET BASE_GEOREF_2 (RENAME=(LOTSECT_ID_2 = LOTSECT_ID LOTMANZ_ID_2 = LOTMANZ_ID LOTLOTE_ID_2 = LOTLOTE_ID BARMANPRE_2 = BARMANPRE));
RUN;

