
LIBNAME CC_con "\\prowin04\Prog_COMUN\ESTADISTICA\input\CC";
%LET RUTA_INFO = \\prowin04\Prog_COMUN\ESTADISTICA\input;
/*%LET VIG = 19;*/
/*%LET MES = 01;*/
%INCLUDE "\\prowin04\Prog_COMUN\ESTADISTICA\source\220404_PROGRAMA_USO_TRADUCTOR.SAS";

%INCLUDE "\\prowin04\Prog_COMUN\ESTADISTICA\source\Cien_Cuadras_MACRO.sas";



%LECTURA_MASIVA_CC(22, VALS=02);
/*%LECTURA_MASIVA_CC(19, VALS=01 02 03 04);*/
/*%LECTURA_MASIVA_CC(20, VALS=01 02 03 04);*/
/*%LECTURA_MASIVA_CC(21, VALS=01 02 03 04);*/



/*data DATABASE_IDENTIFICADOR_19;*/
/*set odbclib.PREDIO_IDENTIFICADOR_2019;*/
/*KEEP CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO CODIGO_CONSTRUCCION CODIGO_RESTO CODIGO_TRADUCTOR;*/
/*RUN;*/
/**/
/*data DATABASE_IDENTIFICADOR_20;*/
/*set odbclib.PREDIO_IDENTIFICADOR_2020;*/
/*KEEP CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO CODIGO_CONSTRUCCION CODIGO_RESTO CODIGO_TRADUCTOR;*/
/*RUN;*/

data DATABASE_IDENTIFICADOR_22;
set odbclib.PREDIO_IDENTIFICADOR_2022;
KEEP CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO CODIGO_CONSTRUCCION CODIGO_RESTO CODIGO_TRADUCTOR;
RUN;

/**/
/*data DATABASE_IDENTIFICADOR_19;*/
/*set odbclib.PREDIO_IDENTIFICADOR_2019;*/
/*KEEP CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO CODIGO_CONSTRUCCION CODIGO_RESTO CODIGO_TRADUCTOR;*/
/*RUN;*/

/*data DATABASE_IDENTIFICADOR_20;*/
/*set odbclib.PREDIO_IDENTIFICADOR_2020;*/
/*KEEP CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO CODIGO_CONSTRUCCION CODIGO_RESTO CODIGO_TRADUCTOR;*/
/*RUN;*/
/**/
/*data DATABASE_IDENTIFICADOR_21;*/
/*set odbclib.PREDIO_IDENTIFICADOR_2021;*/
/*KEEP CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO CODIGO_CONSTRUCCION CODIGO_RESTO CODIGO_TRADUCTOR;*/
/*RUN;*/
/**/
DATA DATABASE_HISTORICO;
SET ODBCLIB.HISTORICO_IDENTIFICADOR_SIIC;
IF CODIGO_MUTACION = 94;
RUN;


%macro TRADUCE_DIR(vig, mes);
DATA  CC_&VIG._&MES._OF_ALL;
SET  CC_&VIG._&MES._OF_ALL;DIRECCION = tranwrd(DIRECCION, '�', 'A');
DIRECCION = tranwrd(DIRECCION, '�', 'A');
DIRECCION = tranwrd(DIRECCION, '�', 'A');
DIRECCION = tranwrd(DIRECCION, '�', 'A');
DIRECCION = tranwrd(DIRECCION, '�', 'A');
DIRECCION = tranwrd(DIRECCION, '�', 'A');
DIRECCION = tranwrd(DIRECCION, '�', 'A');
DIRECCION = tranwrd(DIRECCION, '�', 'C');
DIRECCION = tranwrd(DIRECCION, '�', 'E');
DIRECCION = tranwrd(DIRECCION, '�', 'E');
DIRECCION = tranwrd(DIRECCION, '�', 'E');
DIRECCION = tranwrd(DIRECCION, '�', 'E');
DIRECCION = tranwrd(DIRECCION, '�', 'I');
DIRECCION = tranwrd(DIRECCION, '�', 'I');
DIRECCION = tranwrd(DIRECCION, '�', 'I');
DIRECCION = tranwrd(DIRECCION, '�', 'I');
DIRECCION = tranwrd(DIRECCION, '�', 'D');
DIRECCION = tranwrd(DIRECCION, '�', 'N');
DIRECCION = tranwrd(DIRECCION, '�', 'O');
DIRECCION = tranwrd(DIRECCION, '�', 'O');
DIRECCION = tranwrd(DIRECCION, '�', 'O');
DIRECCION = tranwrd(DIRECCION, '�', 'O');
DIRECCION = tranwrd(DIRECCION, '�', 'O');
DIRECCION = tranwrd(DIRECCION, '�', 'O');
DIRECCION = tranwrd(DIRECCION, '�', 'U');
DIRECCION = tranwrd(DIRECCION, '�', 'U');
DIRECCION = tranwrd(DIRECCION, '�', 'U');
DIRECCION = tranwrd(DIRECCION, '�', 'U');
DIRECCION = tranwrd(DIRECCION, '�', 'Y');
DIRECCION = tranwrd(DIRECCION, 'BOGOTA', ' ');
DIRECCION = tranwrd(DIRECCION, 'COLOMBIA', ' ');
DIRECCION = tranwrd(DIRECCION, 'CUNDINAMARCA', ' ');
DIRECCION = tranwrd(DIRECCION, 'N�', ' ');
DIRECCION = tranwrd(DIRECCION, '�', ' ');
DIRECCION = tranwrd(DIRECCION, '"', ' ');
DIRECCION = tranwrd(DIRECCION, "'", ' ');
DIRECCION = tranwrd(DIRECCION, "[", '');
DIRECCION = tranwrd(DIRECCION, "]", '');
DIRECCION = tranwrd(DIRECCION, ":", '');
DIRECCION = tranwrd(DIRECCION, "/", '');
ID = _n_;
DIR_1=SUBSTR(DIRECCION,1,80);
RUN;
/**/
/*DATA M2_&VIG._&MES._OF_1;*/
/*SET M2_&VIG._&MES._OF;*/
/*IF ID < 5000;*/
/*RUN;*/

%FUENTE(CC_&VIG._&MES._OF_ALL, "CC", DIR_1, DATABASE_IDENTIFICADOR_&VIG., DATABASE_HISTORICO);

data CC_CON.traductor_CC_&VIG._&MES.;
SET GEOREF;
CODIGO_SECTOR = CAT(CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, CODIGO_CONSTRUCCION, CODIGO_RESTO);
RUN;

proc delete data=WORK.GEOREF; run;

%MEND TRADUCE_DIR;

%TRADUCE_DIR(22, 02);

