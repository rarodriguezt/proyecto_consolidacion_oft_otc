
/***  SE CREA LA LIBRERIA DE PROCESAMIENTO DE INFORMACI�N  ***/

LIBNAME FR_21 "\\prowin04\OTC\FincaRaiz\2021";
LIBNAME FR_20 "\\prowin04\OTC\FincaRaiz\2020";
LIBNAME FR_19 "\\prowin04\OTC\FincaRaiz\2019";
LIBNAME FR_14_18 "\\prowin04\OTC\FincaRaiz\2014_2018";

LIBNAME GI_21 "\\prowin04\OTC\GaleriaInmobiliaria\2021";
LIBNAME GI_20 "\\prowin04\OTC\GaleriaInmobiliaria\2020";
LIBNAME GI_19 "\\prowin04\OTC\GaleriaInmobiliaria\2019";
LIBNAME GI_18 "\\prowin04\OTC\GaleriaInmobiliaria\2018";
LIBNAME GI_17 "\\prowin04\OTC\GaleriaInmobiliaria\2017";

LIBNAME OIC_21 "\\prowin04\OTC\OIC\2021";
LIBNAME OIC_20 "\\prowin04\OTC\OIC\2020";
LIBNAME OIC_19 "\\prowin04\OTC\OIC\2019";
LIBNAME OIC_18 "\\prowin04\OTC\OIC\2018";
LIBNAME OIC_17 "\\prowin04\OTC\OIC\2017";

LIBNAME PR_18_21 "\\prowin04\OTC\PROPERATI\2018_2021";
LIBNAME FOCA "\\prowin04\OTC\FOCA";
LIBNAME BASE_OS "\\prowin04\OTC\BASE_OF_SEC";


/***  SE AJUSTAN LAS VARIABLES ESTRATO (FUENTE VS SIIC) Y CLASE DE PREDIO (SE VUELVE A RECALCULAR)   ***/

DATA BASE_HIST_17_21_CONSOLIDADA_5 ; /*129.529*/
SET  BASE_OS.BASE_HIST_17_21_CONSOLIDADA_FIN;

IF TIPO_INMUEBLE NOT = '';
IF PRECIO NOT IN (.,0);

IF ESTRATO = 100 THEN ESTRATO = 0;ELSE ESTRATO = ESTRATO;
IF ESTRATO_SIIC IN (.,0) THEN ESTRATO_1 = ESTRATO; ELSE ESTRATO_1 = ESTRATO_SIIC;

IF TIPO_INMUEBLE IN('APARTAMENTO           ', 'LOCAL                 ', 'OFICINA               ') THEN
   CLASE_PREDIO = 'P';ELSE CLASE_PREDIO = 'N';
IF CLASE_PREDIO_SIIC = '' THEN CLASE_PREDIO_1 = CLASE_PREDIO; ELSE CLASE_PREDIO_1 = CLASE_PREDIO_SIIC;
IF CLASE_PREDIO_1 = '' AND TIPO_INMUEBLE IN ('APARTAMENTO           ','LOCAL                 ','OFICINA               ')THEN
   CLASE_PREDIO_1 = 'P';
IF CLASE_PREDIO_1 = '' THEN CLASE_PREDIO_2 = 'N';ELSE CLASE_PREDIO_2 = CLASE_PREDIO_1;
IF CLASE_PREDIO_SIIC = 'P' THEN CLASE_PREDIO_2 = CLASE_PREDIO_SIIC;
IF CLASE_PREDIO = 'P' THEN CLASE_PREDIO_3 = CLASE_PREDIO; ELSE CLASE_PREDIO_3 = CLASE_PREDIO_2;
IF TIPO_INMUEBLE      = 'LOTE                  ' THEN CLASE_PREDIO_3 = 'N'; 
IF TIPO_OFERTA NOT = ''; 
A�O = YEAR(FECHA);
MES = MONTH (FECHA);

IF A�O NOT = '';
IF MES IN (1,2,3) 		THEN TRIMESTRE =1;
IF MES IN (4,5,6) 		THEN TRIMESTRE =2;
IF MES IN (7,8,9) 		THEN TRIMESTRE =3;
IF MES IN (10,11,12) 	THEN TRIMESTRE =4;

/***  SE AJUSTAN LA FECHA AL A�O DE REPORTE A FOCA   ***/

IF FECHA >= '1Jan2021'd;
IF NOMBRE_FUENTE NOT = 'OIC                 ';

/***  SE AJUSTAN POR FUENTE LOS MESES QUE SE VAN A REPORTAR A FOCA   ***/

IF NOMBRE_FUENTE = 'FINCA RAIZ          ' AND MES > 7 THEN MARCA_FOCA = 1;
IF NOMBRE_FUENTE = 'GALERIA INMOBILIARIA' AND MES > 6 THEN MARCA_FOCA = 1;
IF NOMBRE_FUENTE = 'PROPERATI           ' AND MES > 5 THEN MARCA_FOCA = 1;
IF MARCA_FOCA = 1;
DROP ESTRATO ESTRATO_SIIC CLASE_PREDIO CLASE_PREDIO_1 CLASE_PREDIO_2 AREA_CONSTRUIDA_SIIC AREA_TERRENO_SIIC;
RUN;


/***  SE INCLUYE VARIABLE AREA_CONSTRUIDA_SIIC AREA_TERRENO_SIIC PISO_SIIC PARA IDENTIFICAR PREDIO EN SIIC CON AREAS SIMILAR ***/

DATA PREDIO_FINAL;
SET  COMUN.PREDIO_FINAL_2021;
BARMANPRE=COMPRESS(CODIGO_BARRIO||CODIGO_MANZANA||CODIGO_PREDIO);
AREA_CONSTRUIDA_SIIC = AREA_CONSTRUIDA;
AREA_TERRENO_SIIC = AREA_TERRENO;
PISO_SIIC = SUBSTR(CODIGO_RESTO,1,2)*1;
KEEP BARMANPRE AREA_CONSTRUIDA_SIIC AREA_TERRENO_SIIC PISO_SIIC CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO CODIGO_CONSTRUCCION CODIGO_RESTO DIRECCION_REAL;
RUN;

PROC SORT DATA= PREDIO_FINAL; BY BARMANPRE; RUN;
PROC SORT DATA= BASE_HIST_17_21_CONSOLIDADA_5; BY BARMANPRE; RUN;

DATA BASE_HIST_17_21_CONSOLIDADA_6;/*1.487.671*/
MERGE BASE_HIST_17_21_CONSOLIDADA_5(IN=A) PREDIO_FINAL(IN=B); 
IF A;
BY BARMANPRE;
RUN;

/***  SE SELECCIONAN LOS REGISTROS CON CLASE_PREDIO IGUAL A N  ***/

DATA BASE_HIST_17_21_CONSOLIDADA_6_1; /*21.561*/
SET BASE_HIST_17_21_CONSOLIDADA_6;
IF CLASE_PREDIO_3 ='N';
AREA_CONSTRUIDA_1 = AREA_CONSTRUIDA;
DROP AREA_CONSTRUIDA;
RUN;

PROC SORT NODUPKEY DATA= BASE_HIST_17_21_CONSOLIDADA_6_1; /*21.558*/
BY FUENTE NOMBRE_FUENTE IDENTIFICADOR TIPO_OFERTA FECHA A�O DIRECCION TIPO_INMUEBLE PRECIO VALOR_ADMINISTRACION 
          ESTRATO_1 NUMERO_GARAJES TIPO_GARAJES NUMERO_ALCOBAS NUMERO_BA_OS NUMERO_PISOS AREA_CONSTRUIDA_1 AREA_DE_TERRENO
          ANTIGUEDAD_INMUEBLE BARMANPRE CODIGO_LOCALIDAD NOMBRE_LOCALIDAD CODIGO_UPZ NOMBRE_UPZ CODIGO_BARRIO NOMBRE_BARRIO
		  MARCADOR_GEO CLASE_PREDIO_SIIC CODIGO_DESTINO_SIIC CODIGO_USOP_SIIC VALOR_AVALUO_SIIC ZHF FECHA_CALIFICACION
          CODIGO_BARRIO CODIGO_MANZANA CODIGO_PREDIO CODIGO_CONSTRUCCION CODIGO_RESTO DIRECCION_REAL;
RUN;

/***  SE SELECCIONAN LOS REGISTROS CON CLASE_PREDIO IGUAL A P  ***/

DATA BASE_HIST_17_21_CONSOLIDADA_6_2; /*1.466.110*/
SET BASE_HIST_17_21_CONSOLIDADA_6;
IF CLASE_PREDIO_3 ='P';
RUN;

/***  SE SELECCIONAN LOS REGISTROS CON NUMERO DE PISO IGUAL EN FR Y SIIC  ***/

DATA BASE_HIST_17_21_CONSOLIDADA_6_21; /*115.766*/
SET BASE_HIST_17_21_CONSOLIDADA_6_2;
PRUEBA =  PISO_SIIC - NUMERO_PISOS;
METODOLOGIA= 'PISO/AREA';
IF PRUEBA = 0;
RUN;

/***  SE SELECCIONAN LAS OFERETAS CON INDICE MENOR IGUAL A 10% EN AREA CONTRUIDA  ***/

DATA BASE_HIST_17_21_CONSOLIDADA_6_22; /*115.766*/
SET BASE_HIST_17_21_CONSOLIDADA_6_21;
AC_AFR=ABS((AREA_CONSTRUIDA -  AREA_CONSTRUIDA_SIIC)/AREA_CONSTRUIDA_SIIC);/*CALCULA UN INDICE*/
IF AC_AFR <= 0.1 AND AC_AFR > 0 THEN AREA_CONSTRUIDA_1 = AREA_CONSTRUIDA_SIIC;ELSE AREA_CONSTRUIDA_1 = AREA_CONSTRUIDA;
CONDICION_PISO = 'IGUAL';
RUN;

/***  SE ORDENA POR CODIGO DE SECTOR E INDICE (ASCENDENTE)Y SE SELECCIONA EL PRIMER REGISTRO  ***/

PROC SORT DATA=BASE_HIST_17_21_CONSOLIDADA_6_22; BY IDENTIFICADOR BARMANPRE AC_AFR ;RUN;

DATA BASE_HIST_17_21_CONSOLIDADA_6_23; /*34.323*/
SET BASE_HIST_17_21_CONSOLIDADA_6_22;
BY IDENTIFICADOR BARMANPRE;
IF FIRST.IDENTIFICADOR AND FIRST.BARMANPRE THEN MARCA_CODSEC=1;  ELSE MARCA_CODSEC=0;
IF MARCA_CODSEC=1;
RUN;

/***  SE SELECCIONAN LOS REGISTROS CON NUMERO DE PISO IGUAL EN FR Y SIIC  ***/

DATA BASE_HIST_17_21_CONSOLIDADA_6_24; /*1.350.344*/
SET BASE_HIST_17_21_CONSOLIDADA_6_2;
PRUEBA =  PISO_SIIC - NUMERO_PISOS;
METODOLOGIA= 'AREA';
IF PRUEBA NOT = 0;
RUN;

/***  SE SELECCIONAN LAS OFERETAS CON INDICE MENOR IGUAL A 10% EN AREA CONTRUIDA  ***/

DATA BASE_HIST_17_21_CONSOLIDADA_6_25;
SET BASE_HIST_17_21_CONSOLIDADA_6_24;
AC_AFR=ABS((AREA_CONSTRUIDA -  AREA_CONSTRUIDA_SIIC)/AREA_CONSTRUIDA_SIIC);/*CALCULA UN INDICE*/
IF AC_AFR <= 0.1 AND AC_AFR > 0 THEN AREA_CONSTRUIDA_1 = AREA_CONSTRUIDA_SIIC;ELSE AREA_CONSTRUIDA_1 = AREA_CONSTRUIDA;
CONDICION_PISO = 'DIFERENTE';
RUN;

/***  SE ORDENA POR CODIGO DE SECTOR E INDICE (ASCENDENTE)Y SE SELECCIONA EL PRIMER REGISTRO  ***/

PROC SORT DATA=BASE_HIST_17_21_CONSOLIDADA_6_25; BY IDENTIFICADOR BARMANPRE AC_AFR ;RUN;

DATA BASE_HIST_17_21_CONSOLIDADA_6_26; /*79.981*/
SET BASE_HIST_17_21_CONSOLIDADA_6_25;
BY IDENTIFICADOR BARMANPRE;
IF FIRST.IDENTIFICADOR AND FIRST.BARMANPRE THEN MARCA_CODSEC=1;  ELSE MARCA_CODSEC=0;
IF MARCA_CODSEC=1;

RUN;

/***  SE AGRUPAN LAS BASES CON SELECCION DE AREA SEG�N INDICE PARA CLASE DE PREDIO P ***/

DATA BASE_HIST_17_21_CONSOLIDADA_6_27; /*114.304*/
SET BASE_HIST_17_21_CONSOLIDADA_6_23 BASE_HIST_17_21_CONSOLIDADA_6_26;
DROP AREA_CONSTRUIDA MARCA_CODSEC CONDICION_PISO METODOLOGIA AC_AFR PRUEBA;
RUN;

/***  SE AGRUPAN LAS BASES CON SELECCION DE AREA SEG�N INDICE PARA CLASE DE PREDIO P Y N   ***/

DATA BASE_HIST_17_21_CONSOLIDADA_7;
SET BASE_HIST_17_21_CONSOLIDADA_6_1 BASE_HIST_17_21_CONSOLIDADA_6_27;
IF AREA_CONSTRUIDA_1 = . THEN AREA_CONSTRUIDA_1 = 0;
AREA_CONSTRUIDA = AREA_CONSTRUIDA_1;
DROP AREA_CONSTRUIDA_1;
RUN;

/***  SE INCLUYE VARIABLE VETUSTEZ ***/

DATA VETUSTEZ;
SET ODBCLIB.VISTA_CALIFICACION_2021;
BARMANPRE=COMPRESS(CODIGO_BARRIO||CODIGO_MANZANA||CODIGO_PREDIO);
KEEP BARMANPRE VETUSTEZ ;
RUN;

PROC SORT DATA= VETUSTEZ; BY BARMANPRE VETUSTEZ; RUN;

PROC SORT NODUPKEY DATA= VETUSTEZ; BY BARMANPRE; RUN;

PROC SORT DATA= VETUSTEZ; BY BARMANPRE; RUN;
PROC SORT DATA= BASE_HIST_17_21_CONSOLIDADA_7; BY BARMANPRE; RUN;

DATA BASE_HIST_17_21_CONSOLIDADA_8;/*135.862*/
MERGE BASE_HIST_17_21_CONSOLIDADA_7(IN=A) VETUSTEZ(IN=B); 
IF A;
BY BARMANPRE;
RUN;

/***  SE INCLUYE VARIABLE ATIPICO, SDP Y BIENES DE INTERES CULTURAL ***/

PROC SORT DATA= GI_20.ESTRATO_ATIPICO; BY BARMANPRE; RUN;
PROC SORT DATA= GI_20.ESTRATO_SDP; BY BARMANPRE; RUN;
PROC SORT DATA= GI_20.BIENES_DE_INTERES_CULTURAL; BY BARMANPRE; RUN;
PROC SORT DATA= BASE_HIST_17_21_CONSOLIDADA_8; BY BARMANPRE; RUN;

DATA BASE_HIST_17_21_CONSOLIDADA_9; /*135.862*/
MERGE BASE_HIST_17_21_CONSOLIDADA_8(IN=A) GI_20.ESTRATO_ATIPICO(IN=B) GI_20.BIENES_DE_INTERES_CULTURAL(IN=C) GI_20.ESTRATO_SDP(IN=D); 
IF A;
BY BARMANPRE;
RUN;

/***  SE AJUSTA EL ESTRATO Y SE CALCULA VETUSTEZ ***/

DATA BASE_HIST_17_21_CONSOLIDADA_10;/*135.862*/
SET BASE_HIST_17_21_CONSOLIDADA_9;
IF ESTRATO_1 IN ( .,0) THEN ESTRATO_1 = ESTRATO_ATIPICO;
IF ESTRATO_1 IN ( .,0) THEN ESTRATO_1 = ESTRATO_SDP;
ESTRATO = ESTRATO_1;

IF ANTIGUEDAD_INMUEBLE > 0 THEN VETUSTEZ_1= 2021*1 - ANTIGUEDAD_INMUEBLE; ELSE VETUSTEZ_1 = "";
IF VETUSTEZ = . THEN VETUSTEZ_2 = VETUSTEZ_1; ELSE VETUSTEZ_2 = VETUSTEZ;
IF IIC = 'SI' THEN VALIDA_IIC = 'NO VALIDO';ELSE VALIDA_IIC  = 'VALIDO   '; 

DROP ESTRATO_1 ESTRATO_ATIPICO ESTRATO_SDP IIC VETUSTEZ VETUSTEZ_1 ANTIGUEDAD_INMUEBLEN;
RUN;

/***  SE AJUSTA LAS VARIABLES ESTRATO Y AREA DE TERRENO, SE CALCULAN VALOR_M2_TERRENO VALOR_M2_INTEGRAL E 
      INDICE DE CONSTRUCCION ***/

DATA BASE_HIST_17_21_CONSOLIDADA_11; /*135.862*/
SET BASE_HIST_17_21_CONSOLIDADA_10;

/***  VALIDADOR DE PREDIO POR BARMANPRE  ***/

IF BARMANPRE  = '' THEN VALIDA_BARMANPRE  = 'NO VALIDO';ELSE VALIDA_BARMANPRE  = 'VALIDO'; 

/***  VALIDADOR PREDIOS QUE NO CRUZARON CON EL SIIC  ***/

IF CLASE_PREDIO_SIIC = '' AND CODIGO_DESTINO_SIIC = '' THEN VALIDA_CRUCE_SIIC = 'NO VALIDO'; ELSE VALIDA_CRUCE_SIIC = 'VALIDO';

/***  VALIDADOR AREA CONSTRUIDA  ***/

FECHA_CALIFICACION1 = datepart(FECHA_CALIFICACION);
FORMAT FECHA_CALIFICACION1 ddmmyy10. ;
FECHA_CALIFICACION2=intnx("day",FECHA_CALIFICACION1,731);
FORMAT FECHA_CALIFICACION2 ddmmyy10.;
IF FECHA_CALIFICACION2 > FECHA THEN MARCA_FECHA_CAL = 1;

IF TIPO_INMUEBLE = 'LOTE                  ' THEN AREA_CONSTRUIDA = 0;

IF CLASE_PREDIO_3 = 'N' AND TIPO_INMUEBLE NOT IN ('LOTE                  ', 'FINCA              ') AND  AREA_CONSTRUIDA IN(0,.)AND 
   MARCA_FECHA_CAL = 1 THEN AREA_CONSTRUIDA_1 = AREA_CONSTRUIDA_SIIC; ELSE AREA_CONSTRUIDA_1 = AREA_CONSTRUIDA;

IF CLASE_PREDIO_3 = 'N' AND TIPO_INMUEBLE NOT IN ('LOTE                  ', 'FINCA              ') AND  AREA_CONSTRUIDA_1 NOT IN (0,.)
   THEN VALIDA_AREA_CONST = 'VALIDO   ';ELSE VALIDA_AREA_CONST = 'NO VALIDO';

IF CLASE_PREDIO_3 = 'P' THEN VALIDA_AREA_CONST = 'VALIDO   ';

IF TIPO_INMUEBLE NOT IN('LOTE                  ','FINCA              ') THEN VAR_AREA_CONSTRUIDA = ABS((AREA_CONSTRUIDA_1/AREA_CONSTRUIDA_SIIC)*100-100);

IF TIPO_INMUEBLE NOT IN('LOTE                  ','FINCA              ') AND VAR_AREA_CONSTRUIDA => 0 
AND VAR_AREA_CONSTRUIDA <= 10 THEN VALIDA_DIF_AREA_CONST = 'VALIDO   '; ELSE VALIDA_DIF_AREA_CONST = 'NO VALIDO';

IF TIPO_INMUEBLE IN('LOTE                  ','FINCA              ') THEN VALIDA_DIF_AREA_CONST = 'VALIDO   ';

IF CLASE_PREDIO_3 = 'P' THEN VALOR_M2_INTEGRAL = PRECIO / AREA_CONSTRUIDA_1;

/***  VALIDADOR AREA TERRENO  ***/

IF CLASE_PREDIO_3 = 'N' AND AREA_DE_TERRENO IN(0,.) THEN AREA_DE_TERRENO_1 = AREA_TERRENO_SIIC;ELSE AREA_DE_TERRENO_1 = AREA_DE_TERRENO;

IF CLASE_PREDIO_3 = 'N' THEN VAR_AREA_TERRENO = ABS((AREA_DE_TERRENO_1/AREA_TERRENO_SIIC)*100-100);

IF CLASE_PREDIO_3 = 'N' AND AREA_DE_TERRENO_1 IN(0,.) OR VAR_AREA_TERRENO => 10 THEN VALIDA_DIF_AREA_TERR ='NO VALIDO';ELSE VALIDA_DIF_AREA_TERR = 'VALIDO   ';

IF CLASE_PREDIO_3 = 'P' THEN VALIDA_DIF_AREA_TERR = 'VALIDO   ';

IF TIPO_INMUEBLE IN ('LOTE                  ', 'FINCA              ') AND TIPO_OFERTA   ='VENTA   ' THEN VALOR_M2_TERRENO = PRECIO / AREA_DE_TERRENO;


/***  CALCULA VARIACION PRECIO VS AVALUO  ***/

IF TIPO_OFERTA   ='VENTA   ' THEN VAR_AVALUO = (PRECIO/VALOR_AVALUO_SIIC)*100-100;

CLASE_PREDIO = CLASE_PREDIO_3;
VETUSTEZ= VETUSTEZ_2;

DROP CLASE_PREDIO_3 VETUSTEZ_2 AREA_CONSTRUIDA FECHA_CALIFICACION1 FECHA_CALIFICACION2 MARCA_FECHA_CAL AREA_DE_TERRENO;
RUN;


/***  SE PEGAN PREDIOS_DISTRITO(IDU-DADEP)  ***/

DATA LOTES_DISTRITO;
SET FR_21.LOTES_DISTRITO_MAY21;
KEEP BARMANPRE LOTDISTRIT;
RUN;

PROC SORT DATA= BASE_HIST_17_21_CONSOLIDADA_9; BY BARMANPRE; RUN;
PROC SORT DATA= LOTES_DISTRITO; BY BARMANPRE; RUN;

DATA BASE_HIST_17_21_CONSOLIDADA_12; /*135.862*/
MERGE BASE_HIST_17_21_CONSOLIDADA_11(IN=A) LOTES_DISTRITO(IN=B); 
IF A;
BY BARMANPRE;
RUN;


/***  VALIDADOR DE USOS  ***/

PROC SQL;
	CREATE TABLE BASE_HIST_17_21_CONSOLIDADA_13 AS	
		SELECT *,
		CASE
WHEN CODIGO_USOP_SIIC IN ('002') AND TIPO_INMUEBLE ='CASA                  ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('002') AND TIPO_INMUEBLE ='CASA_LOTE             ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('002') AND TIPO_INMUEBLE ='CASA_USO_MIXTO        ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('002') AND TIPO_INMUEBLE ='EDIFICIO              ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('003') AND TIPO_INMUEBLE ='LOCAL                 ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('003') AND TIPO_INMUEBLE ='CASA_USO_MIXTO        ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('003') AND TIPO_INMUEBLE ='CASA                  ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('003') AND TIPO_INMUEBLE ='EDIFICIO              ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('004') AND TIPO_INMUEBLE ='LOCAL                 ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('004') AND TIPO_INMUEBLE ='CASA_USO_MIXTO        ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('004') AND TIPO_INMUEBLE ='CASA                  ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('004') AND TIPO_INMUEBLE ='EDIFICIO              ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('001') AND TIPO_INMUEBLE ='CASA                  ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('001') AND TIPO_INMUEBLE ='CASA_LOTE             ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('001') AND TIPO_INMUEBLE ='CASA_USO_MIXTO        ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('008') AND TIPO_INMUEBLE ='INMUEBLE_COMERCIAL    ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('008') AND TIPO_INMUEBLE ='LOCAL                 ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('008') AND TIPO_INMUEBLE ='BODEGA                ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('023') AND TIPO_INMUEBLE ='INMUEBLE_COMERCIAL    ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('023') AND TIPO_INMUEBLE ='LOCAL                 ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('023') AND TIPO_INMUEBLE ='DOTACIONAL            ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('047') AND TIPO_INMUEBLE ='INMUEBLE_COMERCIAL    ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('047') AND TIPO_INMUEBLE ='LOCAL                 ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('047') AND TIPO_INMUEBLE ='DOTACIONAL            ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('091') AND TIPO_INMUEBLE ='INMUEBLE_COMERCIAL    ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('091') AND TIPO_INMUEBLE ='LOCAL                 ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('091') AND TIPO_INMUEBLE ='BODEGA                ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('096') AND TIPO_INMUEBLE ='EDIFICIO              ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('096') AND TIPO_INMUEBLE ='PARQUEADERO-LAVA_AUTOS' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('096') AND TIPO_INMUEBLE ='GARAJE                ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('015') AND TIPO_INMUEBLE ='OFICINA               ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('015') AND TIPO_INMUEBLE ='DOTACIONAL            ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('021') AND TIPO_INMUEBLE ='EDIFICIO              ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('021') AND TIPO_INMUEBLE ='INMUEBLE_COMERCIAL    ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('022') AND TIPO_INMUEBLE ='DEPOSITO              ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('022') AND TIPO_INMUEBLE ='BODEGA                ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('024') AND TIPO_INMUEBLE ='EDIFICIO              ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('024') AND TIPO_INMUEBLE ='PARQUEADERO-LAVA_AUTOS' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('026') AND TIPO_INMUEBLE ='EDIFICIO              ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('026') AND TIPO_INMUEBLE ='INMUEBLE_COMERCIAL    ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('027') AND TIPO_INMUEBLE ='EDIFICIO              ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('027') AND TIPO_INMUEBLE ='INMUEBLE_COMERCIAL    ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('037') AND TIPO_INMUEBLE ='APARTAMENTO           ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('037') AND TIPO_INMUEBLE ='CASA                  ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('046') AND TIPO_INMUEBLE ='EDIFICIO              ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('046') AND TIPO_INMUEBLE ='INMUEBLE_COMERCIAL    ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('050') AND TIPO_INMUEBLE ='EDIFICIO              ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('050') AND TIPO_INMUEBLE ='PARQUEADERO-LAVA_AUTOS' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('051') AND TIPO_INMUEBLE ='DEPOSITO              ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('051') AND TIPO_INMUEBLE ='BODEGA                ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('056') AND TIPO_INMUEBLE ='INMUEBLE_COMERCIAL    ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('056') AND TIPO_INMUEBLE ='LOCAL                 ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('057') AND TIPO_INMUEBLE ='OFICINA               ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('057') AND TIPO_INMUEBLE ='LOCAL                 ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('060') AND TIPO_INMUEBLE ='INMUEBLE_COMERCIAL    ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('060') AND TIPO_INMUEBLE ='LOCAL                 ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('092') AND TIPO_INMUEBLE ='OFICINA               ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('092') AND TIPO_INMUEBLE ='DOTACIONAL            ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('098') AND TIPO_INMUEBLE ='DEPOSITO              ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('098') AND TIPO_INMUEBLE ='BODEGA                ' THEN 'VALIDO   '

WHEN CODIGO_USOP_SIIC IN ('048','049') AND TIPO_INMUEBLE ='GARAJE                ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('038','090') AND TIPO_INMUEBLE ='APARTAMENTO           ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('019','009','033','097','025','093','011','034','010','028','080','081') AND TIPO_INMUEBLE = 'BODEGA                ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('064','055','017','043','030','065','013','016','035','032','014','053','018','012','044','067','029','036','031','052','066') AND TIPO_INMUEBLE = 'DOTACIONAL            ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('007','042','006','041','094','095','040','039') AND TIPO_INMUEBLE ='LOCAL                 ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('074','073','070','072','071','076','075','077') AND TIPO_INMUEBLE ='OTROS                 ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('082','005') AND TIPO_INMUEBLE ='INMUEBLE_COMERCIAL    ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('020','045') AND TIPO_INMUEBLE ='OFICINA               ' THEN 'VALIDO   '
WHEN CODIGO_USOP_SIIC IN ('058','059','062') AND TIPO_INMUEBLE ='DOTACIONAL            ' THEN 'VALIDO   '
	ELSE 'NO VALIDO'
	END AS VALIDA_USO
	FROM BASE_HIST_17_21_CONSOLIDADA_12;
	QUIT;

DATA BASE_HIST_17_21_CONSOLIDADA_14; /*135.862*/
SET BASE_HIST_17_21_CONSOLIDADA_13;

/***  VALIDADOR DE DESTINO ECONOMICO  ***/

IF CODIGO_DESTINO_SIIC IN ('','04','05','06','07','08','63','65','66','67') THEN VALIDA_DES_ECONOMICO = 'NO VALIDO';ELSE VALIDA_DES_ECONOMICO = 'VALIDO   ';

/***  VALIDADOR ZHF  ***/

ZHF_PREDIO = SUBSTR(ZHF,12,2);
ZHF_SUELOP = SUBSTR(ZHF,1,1);
IF ZHF_PREDIO IN ('','40','41','42','43','44','45','46','47','48','49','51','52','53','54','61','62','63') AND ZHF_SUELOP IN ('','5')THEN VALIDA_ZHF = 'NO VALIDO';ELSE VALIDA_ZHF = 'VALIDO   ';

/***  VALIDADOR DE PREDIOS_DISTRITO(IDU-DADEP)  ***/

IF LOTDISTRIT = 'SI' THEN VALIDA_LOTDISTRIT = 'NO VALIDO';ELSE VALIDA_LOTDISTRIT = 'VALIDO   ';

AREA_CONSTRUIDA = AREA_CONSTRUIDA_1;
AREA_DE_TERRENO = AREA_DE_TERRENO_1;

DROP NUM_LOTE ZHF_PREDIO ZHF_SUELOP AREA_CONSTRUIDA_1 AREA_DE_TERRENO_1;

RUN;


/***  SE VALIDA EL ESTRATO SEGUN LA LOCALIDAD  ***/


PROC SQL;
	CREATE TABLE BASE_HIST_17_21_CONSOLIDADA_15 AS	
		SELECT *,
		CASE
WHEN 	TIPO_INMUEBLE NOT IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ') THEN 'VALIDO'

WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '15' AND ESTRATO  IN(2,3)         THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '12' AND ESTRATO  IN(3,4,5)       THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '07' AND ESTRATO  IN(1,2,3)       THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '18' AND ESTRATO  IN(1,2,3)       THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '02' AND ESTRATO  IN(1,2,3,4,5,6) THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '19' AND ESTRATO  IN(1,2,3)       THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '10' AND ESTRATO  IN(1,2,3,4)     THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '09' AND ESTRATO  IN(2,3,4,5)     THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '08' AND ESTRATO  IN(1,2,3,4)     THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '14' AND ESTRATO  IN(2,3)         THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '16' AND ESTRATO  IN(2,3)         THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '18' AND ESTRATO  IN(1,2,3)       THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '04' AND ESTRATO  IN(1,2,3)       THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '03' AND ESTRATO  IN(1,2,3,4)     THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '11' AND ESTRATO  IN(1,2,3,4,5,6) THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '13' AND ESTRATO  IN(3,4,5)       THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '06' AND ESTRATO  IN(2,3)         THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '01' AND ESTRATO  IN(1,2,3,4,5,6) THEN 'VALIDO'
WHEN 	TIPO_INMUEBLE IN( 'APARTAMENTO           ','CASA                  ','FR_HABITACION         ','FR_CASA_CAMPESTRE     ', 'CASA_LOTE             ')AND CODIGO_LOCALIDAD = '05' AND ESTRATO  IN(1,2)         THEN 'VALIDO'
	ELSE 'NO VALIDO'
	END AS VALIDA_ESTRATO
	FROM BASE_HIST_17_21_CONSOLIDADA_14;
	QUIT;

/*********************************************/
/***  BARMANPRE REPETIDOS EN CADA FUENTES  ***/
/*********************************************/

/***  DEPURACION NPH POR FUENTE   ***/

DATA REPET_FUENTE_NPH; /*21.558 */
SET BASE_HIST_17_21_CONSOLIDADA_15;
IF CLASE_PREDIO = 'N';
RUN;

PROC SORT DATA= REPET_FUENTE_NPH; BY BARMANPRE NOMBRE_FUENTE TIPO_OFERTA TIPO_INMUEBLE DESCENDING FECHA; RUN;

DATA REPET_FUENTE_NPH_1;/*21.558 */
SET REPET_FUENTE_NPH;
LLAVE_AJUSTE_FUENTE=COMPRESS(BARMANPRE||NOMBRE_FUENTE||TIPO_OFERTA||TIPO_INMUEBLE||MES);
RUN;

PROC SORT DATA= REPET_FUENTE_NPH_1; BY LLAVE_AJUSTE_FUENTE; RUN;

DATA REPET_FUENTE_NPH_2; /*21.558*/
SET REPET_FUENTE_NPH_1;
BY LLAVE_AJUSTE_FUENTE;
IF FIRST.LLAVE_AJUSTE_FUENTE THEN MARCA_REPET_FUENTE='NO REPETIDO';  ELSE MARCA_REPET_FUENTE='REPETIDO';
RUN;

PROC SORT DATA= REPET_FUENTE_NPH_2; BY BARMANPRE NOMBRE_FUENTE TIPO_OFERTA TIPO_INMUEBLE FECHA; RUN;

/***  DEPURACION PH ***/

DATA REPET_FUENTE_PH; /*114.304*/
SET BASE_HIST_17_21_CONSOLIDADA_15;
IF CLASE_PREDIO = 'P';
RUN;

PROC SORT DATA= REPET_FUENTE_PH; BY BARMANPRE NOMBRE_FUENTE TIPO_OFERTA TIPO_INMUEBLE DESCENDING FECHA AREA_CONSTRUIDA; RUN;

DATA REPET_FUENTE_PH_1;/*114.304 */
SET REPET_FUENTE_PH;
LLAVE_AJUSTE_FUENTE=COMPRESS(BARMANPRE||TIPO_OFERTA||NOMBRE_FUENTE||TIPO_INMUEBLE||MES||AREA_CONSTRUIDA);
RUN;

PROC SORT DATA= REPET_FUENTE_PH_1; BY LLAVE_AJUSTE_FUENTE; RUN;

DATA REPET_FUENTE_PH_2; /*114.304*/
SET REPET_FUENTE_PH_1;
BY LLAVE_AJUSTE_FUENTE;
IF FIRST.LLAVE_AJUSTE_FUENTE THEN MARCA_REPET_FUENTE='NO REPETIDO';  ELSE MARCA_REPET_FUENTE='REPETIDO';
RUN;

PROC SORT DATA= REPET_FUENTE_PH_2; BY BARMANPRE NOMBRE_FUENTE TIPO_OFERTA TIPO_INMUEBLE FECHA AREA_CONSTRUIDA; RUN;

/***  CONSOLIDA NPH Y PH CON MARCA DE REPETIDOS EN CADA FUENTE  ***/

DATA BASE_HIST_17_21_CONSOLIDADA_16; /*135.862*/
SET REPET_FUENTE_NPH_2 REPET_FUENTE_PH_2;
RUN;/*Fuente de datos:  ODBCLIB.PREDIO_2021 */

/*******************************************/
/***  BARMANPRE REPETIDOS ENTRE FUENTES  ***/
/*******************************************/

/***  DEPURACION NPH ENTRE FUENTE   ***/

DATA REPETIDOS_NPH; /*21.558 */
SET BASE_HIST_17_21_CONSOLIDADA_16;
IF CLASE_PREDIO = 'N';
RUN;

PROC SORT DATA= REPET_FUENTE_NPH; BY BARMANPRE TIPO_OFERTA TIPO_INMUEBLE DESCENDING FECHA; RUN;

DATA REPETIDOS_NPH_1;/* 21.558 */
SET REPETIDOS_NPH;
LLAVE_AJUSTE=COMPRESS(BARMANPRE||TIPO_OFERTA||TIPO_INMUEBLE||MES);
RUN;

PROC SORT DATA= REPETIDOS_NPH_1; BY LLAVE_AJUSTE; RUN;

DATA REPETIDOS_NPH_2; /*21.558*/
SET REPETIDOS_NPH_1;
BY LLAVE_AJUSTE;
IF FIRST.LLAVE_AJUSTE THEN MARCA_REPETIDOS='NO REPETIDO';  ELSE MARCA_REPETIDOS='REPETIDO';
RUN;

PROC SORT DATA= REPETIDOS_NPH_2; BY BARMANPRE TIPO_OFERTA TIPO_INMUEBLE FECHA; RUN;

/***  DEPURACION PH ***/

DATA REPETIDOS_PH; /*114.304*/
SET BASE_HIST_17_21_CONSOLIDADA_16;
IF CLASE_PREDIO = 'P';
RUN;

PROC SORT DATA= REPETIDOS_PH; BY BARMANPRE TIPO_OFERTA TIPO_INMUEBLE DESCENDING FECHA AREA_CONSTRUIDA; RUN;

DATA REPETIDOS_PH_1;/* 114.304 */
SET REPETIDOS_PH;
LLAVE_AJUSTE=COMPRESS(BARMANPRE||TIPO_OFERTA||TIPO_INMUEBLE||MES||AREA_CONSTRUIDA);
RUN;

PROC SORT DATA= REPETIDOS_PH_1; BY LLAVE_AJUSTE; RUN;

DATA REPETIDOS_PH_2; /*114.304*/
SET REPETIDOS_PH_1;
BY LLAVE_AJUSTE;
IF FIRST.LLAVE_AJUSTE THEN MARCA_REPETIDOS='NO REPETIDO';  ELSE MARCA_REPETIDOS='REPETIDO';
RUN;

PROC SORT DATA= REPETIDOS_PH_2; BY BARMANPRE TIPO_OFERTA TIPO_INMUEBLE FECHA AREA_CONSTRUIDA; RUN;

/***  CONSOLIDA NPH Y PH CON MARCA DE REPETIDOS EN CADA FUENTE  ***/

DATA BASE_HIST_17_21_CONSOLIDADA_17; /*135.862*/
SET REPETIDOS_NPH_2 REPETIDOS_PH_2;
DROP LLAVE_AJUSTE LLAVE_AJUSTE_FUENTE;
RUN;


/**************************************/
/*          ESTADISTICOS              */
/**************************************/

DATA BASE_ESTADISTICOS; /*10.008*/
SET BASE_HIST_17_21_CONSOLIDADA_17;
/*IF VALIDA_IIC = 'VALIDO   ';*/
IF VALIDA_BARMANPRE = 'VALIDO   ';
IF VALIDA_CRUCE_SIIC = 'VALIDO   ';
IF VALIDA_AREA_CONST= 'VALIDO   ';
IF VALIDA_DIF_AREA_CONST= 'VALIDO   ';
IF VALIDA_DIF_AREA_TERR= 'VALIDO   ';
IF VALIDA_USO = 'VALIDO   ';
IF VALIDA_DES_ECONOMICO= 'VALIDO   ';
IF VALIDA_ZHF= 'VALIDO   ';
IF VALIDA_LOTDISTRIT= 'VALIDO   ';
IF VALIDA_ESTRATO= 'VALIDO   ';
IF MARCA_REPET_FUENTE='NO REPETIDO';
IF MARCA_REPETIDOS='NO REPETIDO';
RUN;


/***  SE CALCULAN ESTADISTICOS UNIVARIADOS DE LA VARIABLE VALOR_M2_INTEGRAL  ***/

PROC SORT DATA= BASE_ESTADISTICOS; BY ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO ;RUN;

PROC UNIVARIATE DATA= BASE_ESTADISTICOS OUTTABLE=UNI_VALOR_M2_INTEGRAL NOPRINT ;
BY ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO;
VAR VALOR_M2_INTEGRAL;
WHERE VALOR_M2_INTEGRAL NOT IN(.,0);
RUN;

/***  SE CALCULAN ESTADISTICOS UNIVARIADOS DE LA VARIABLE PRECIO  ***/

PROC SORT DATA=BASE_ESTADISTICOS; BY ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO;RUN;

PROC UNIVARIATE DATA= BASE_ESTADISTICOS OUTTABLE=UNI_PRECIO NOPRINT ;
BY ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO;
VAR PRECIO;
WHERE PRECIO NOT IN(.,0);
RUN;


/***  SE CALCULAN ESTADISTICOS UNIVARIADOS DE LA VARIABLE VALOR METRO CUADRADO TERRENO  ***/

PROC SORT DATA= BASE_ESTADISTICOS; BY ESTRATO TIPO_INMUEBLE TIPO_OFERTA;RUN;

PROC UNIVARIATE DATA= BASE_ESTADISTICOS OUTTABLE=UNI_VALOR_M2_TERRENO NOPRINT ;
BY ESTRATO TIPO_INMUEBLE TIPO_OFERTA;
VAR VALOR_M2_TERRENO;
WHERE VALOR_M2_TERRENO NOT IN(.,0);
RUN;

/***  SE CALCULAN ESTADISTICOS UNIVARIADOS DE LA VARIABLE AVALUO  ***/

PROC SORT DATA= BASE_ESTADISTICOS; BY ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO;RUN;

PROC UNIVARIATE DATA= BASE_ESTADISTICOS OUTTABLE=UNI_AVALUO NOPRINT ;
BY ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO;
VAR VALOR_AVALUO_SIIC;
WHERE VALOR_AVALUO_SIIC NOT IN(.,0) AND VAR_AVALUO > -50;
RUN;


/***  SE CALCULAN SACAN LOS PERCENTILES 1 Y 99 PARA IDENTIFICAR OUTLAYERS  ***/

DATA UNI_VALOR_M2_INTEGRAL_1;
SET UNI_VALOR_M2_INTEGRAL;
_P1_VALOR_M2_INTEGRAL = _P1_;
_P99_VALOR_M2_INTEGRAL = _P99_;
_Q1_VALOR_M2_INTEGRAL = _Q1_;
_Q3_VALOR_M2_INTEGRAL = _Q3_;
_NOBS_VALOR_M2_INTEGRAL = _NOBS_;
IQR_VALOR_M2_INTEGRAL = _Q3_VALOR_M2_INTEGRAL - _Q1_VALOR_M2_INTEGRAL;
LI_VALOR_M2_INTEGRAL = _Q1_VALOR_M2_INTEGRAL - 1.5 * IQR_VALOR_M2_INTEGRAL;
LS_VALOR_M2_INTEGRAL = _Q3_VALOR_M2_INTEGRAL + 1.5 * IQR_VALOR_M2_INTEGRAL;
IF CLASE_PREDIO = 'P';
IF _NOBS_ >= 5;
KEEP ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO _P1_VALOR_M2_INTEGRAL _P99_VALOR_M2_INTEGRAL _NOBS_VALOR_M2_INTEGRAL _Q1_VALOR_M2_INTEGRAL _Q3_VALOR_M2_INTEGRAL LI_VALOR_M2_INTEGRAL LS_VALOR_M2_INTEGRAL IQR_VALOR_M2_INTEGRAL;
RUN;


DATA UNI_VALOR_M2_TERRENO_1;
SET UNI_VALOR_M2_TERRENO;
_P1_VALOR_M2_TERRENO = _P1_;
_P99_VALOR_M2_TERRENO = _P99_;
_NOBS_VALOR_M2_TERRENO = _NOBS_;
_Q1_VALOR_M2_TERRENO = _Q1_;
_Q3_VALOR_M2_TERRENO = _Q3_;
IQR_VALOR_M2_TERRENO = _Q3_VALOR_M2_TERRENO - _Q1_VALOR_M2_TERRENO;
LI_VALOR_M2_TERRENO = _Q1_VALOR_M2_TERRENO - 1.5 * IQR_VALOR_M2_TERRENO;
LS_VALOR_M2_TERRENO = _Q3_VALOR_M2_TERRENO + 1.5 * IQR_VALOR_M2_TERRENO;
IF CLASE_PREDIO = 'N';
IF TIPO_OFERTA ='VENTA   ';
IF _NOBS_ >= 5;
KEEP ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO _P1_VALOR_M2_TERRENO _P99_VALOR_M2_TERRENO _NOBS_VALOR_M2_TERRENO _Q1_VALOR_M2_TERRENO _Q3_VALOR_M2_TERRENO LI_VALOR_M2_TERRENO LS_VALOR_M2_TERRENO IQR_VALOR_M2_TERRENO;
RUN;


DATA UNI_PRECIO_1;
SET UNI_PRECIO;
_P1_PRECIO = _P1_;
_P99_PRECIO = _P99_;
_NOBS_PRECIO = _NOBS_;
_Q1_PRECIO = _Q1_;
_Q3_PRECIO = _Q3_;
IQR_PRECIO = _Q3_PRECIO - _Q1_PRECIO;
LI_PRECIO = _Q1_PRECIO - 1.5 * IQR_PRECIO;
LS_PRECIO = _Q3_PRECIO + 1.5 * IQR_PRECIO;
IF _NOBS_ >= 5;
KEEP ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO _P1_PRECIO _P99_PRECIO _NOBS_PRECIO _Q3_PRECIO _Q1_PRECIO LI_PRECIO LS_PRECIO IQR_PRECIO;
RUN;


DATA UNI_AVALUO_1;
SET UNI_AVALUO;
_P1_AVALUO = _P1_;
_P99_AVALUO = _P99_;
_NOBS_AVALUO = _NOBS_;
_Q1_AVALUO = _Q1_;
_Q3_AVALUO = _Q3_;
IQR_AVALUO = _Q3_AVALUO - _Q1_AVALUO;
LI_AVALUO = _Q1_AVALUO - 1.5 * IQR_AVALUO;
LS_AVALUO = _Q3_AVALUO + 1.5 * IQR_AVALUO;
IF _NOBS_ >= 5;
IF TIPO_OFERTA   ='VENTA   ';
KEEP ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO _P1_AVALUO _P99_AVALUO _NOBS_AVALUO _Q3_AVALUO _Q1_AVALUO LI_AVALUO LS_AVALUO IQR_AVALUO;
RUN;

/***  SE ORDENAN LAS BASES UNIVARIADAS PARA CONSOLIDACION  ***/

PROC SORT DATA=UNI_VALOR_M2_INTEGRAL_1;        		BY ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO;RUN;
/*PROC SORT DATA=UNI_VALOR_M2_TERRENO_1;        		BY ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO;RUN;*/
PROC SORT DATA=UNI_PRECIO_1;      					BY ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO;RUN;
PROC SORT DATA=UNI_AVALUO_1;      					BY ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO;RUN;
PROC SORT DATA=BASE_HIST_17_21_CONSOLIDADA_17;  	BY ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO;RUN;

/***  SE PEGAN LAS BASES DE DATOS UNIVARIADAS A FR_FOCA_3  ***/

DATA BASE_HIST_17_21_CONSOLIDADA_18; /*135.862*/
MERGE BASE_HIST_17_21_CONSOLIDADA_17(IN=A) UNI_VALOR_M2_INTEGRAL_1 (IN=B) /*UNI_VALOR_M2_TERRENO_1(IN=C)*/ UNI_PRECIO_1(IN=D) UNI_AVALUO_1(IN=E);  
IF A;
BY ESTRATO TIPO_INMUEBLE TIPO_OFERTA CLASE_PREDIO;
RUN;

/***  SE IDENTIFICAN LOS OUTLAYERS  ***/

DATA BASE_HIST_17_21_CONSOLIDADA_19 /*(DROP= OUTLAYER_1 OUTLAYER_2 OUTLAYER_3  OUTLAYER_4 OUTLAYER_5 OUTLAYER_6)*/; /*1.098.887*/
SET BASE_HIST_17_21_CONSOLIDADA_18;

/***  OUTLAYERS  VALOR_M2_INTEGRAL   ***/

IF CLASE_PREDIO = 'P' AND  _P99_VALOR_M2_INTEGRAL NOT IN (.,0) THEN MARCA_VLR_INT = 1;ELSE MARCA_VLR_INT = 0;

IF MARCA_VLR_INT = 1 AND (VALOR_M2_INTEGRAL < _P1_VALOR_M2_INTEGRAL OR  VALOR_M2_INTEGRAL > _P99_VALOR_M2_INTEGRAL) THEN OUTLAYER_1 = 'VALOR_M2_INTEGR';
IF MARCA_VLR_INT = 1 AND (VALOR_M2_INTEGRAL < LI_VALOR_M2_INTEGRAL  OR  VALOR_M2_INTEGRAL > LS_VALOR_M2_INTEGRAL)   THEN OUTLAYER_1 = 'VALOR_M2_INTEGR';

/***  OUTLAYERS  VALOR_M2_TERRENO   

IF VALOR_M2_TERRENO < _P1_VALOR_M2_TERRENO OR  VALOR_M2_TERRENO > _P99_VALOR_M2_TERRENO THEN OUTLAYER_2 = 'VALOR_M2_TERREN';
IF VALOR_M2_TERRENO < LI_VALOR_M2_TERRENO  OR  VALOR_M2_TERRENO > LS_VALOR_M2_TERRENO   THEN OUTLAYER_2 = 'VALOR_M2_TERREN';
***/

/***  OUTLAYERS  PRECIO   ***/

IF _P99_VALOR_M2_INTEGRAL NOT IN (.,0) AND (PRECIO < _P1_PRECIO OR  PRECIO > _P99_PRECIO) THEN OUTLAYER_3 = 'PRECIO         ';
IF _P99_VALOR_M2_INTEGRAL NOT IN (.,0) AND (PRECIO < LI_PRECIO  OR  PRECIO > LS_PRECIO)   THEN OUTLAYER_3 = 'PRECIO         ';

/***  OUTLAYERS  AVALUO   ***/

IF TIPO_OFERTA ='VENTA   ' AND VAR_AVALUO > -50 AND VALOR_AVALUO_SIIC NOT IN(.,0) AND _P99_AVALUO NOT IN (.,0) THEN MARCA_AVALUO = 1 ; ELSE MARCA_AVALUO = 0;
IF MARCA_AVALUO = 1 AND (VALOR_AVALUO_SIIC < _P1_AVALUO OR  VALOR_AVALUO_SIIC > _P99_AVALUO) THEN OUTLAYER_4 = 'AVALUO         ';

DROP MARCA_AVALUO MARCA_VLR_INT;
RUN;

PROC SQL; CREATE TABLE FOCA.BASE_FOCA_MAY21_OCT21 
AS SELECT FUENTE, NOMBRE_FUENTE, IDENTIFICADOR, TIPO_INMUEBLE, TIPO_OFERTA, FECHA, A�O, MES, TRIMESTRE, DIRECCION, 
          PRECIO, AREA_CONSTRUIDA, AREA_DE_TERRENO, VALOR_ADMINISTRACION, NUMERO_GARAJES, TIPO_GARAJES, NUMERO_ALCOBAS, 
          NUMERO_BA_OS, NUMERO_PISOS, ANTIGUEDAD_INMUEBLE, ESTRATO, CLASE_PREDIO, VETUSTEZ, AREA_CONSTRUIDA_SIIC, 
          AREA_TERRENO_SIIC, PISO_SIIC, CLASE_PREDIO_SIIC, CODIGO_DESTINO_SIIC, CODIGO_USOP_SIIC, VALOR_AVALUO_SIIC, 
          ZHF, FECHA_CALIFICACION, VALOR_M2_INTEGRAL, VALOR_M2_TERRENO, LOTDISTRIT, BARMANPRE, MARCADOR_GEO, CODIGO_LOCALIDAD, 
          NOMBRE_LOCALIDAD, CODIGO_UPZ, NOMBRE_UPZ, CODIGO_BARRIO, NOMBRE_BARRIO, VALIDA_BARMANPRE, VALIDA_CRUCE_SIIC, 
          VALIDA_USO, VALIDA_DES_ECONOMICO, VALIDA_ZHF, VALIDA_LOTDISTRIT, VALIDA_IIC, VALIDA_ESTRATO, VALIDA_AREA_CONST, 
          VAR_AREA_CONSTRUIDA, VALIDA_DIF_AREA_CONST, VAR_AREA_TERRENO, VALIDA_DIF_AREA_TERR, VAR_AVALUO, MARCA_REPET_FUENTE, 
          MARCA_REPETIDOS, _P1_VALOR_M2_INTEGRAL, _P99_VALOR_M2_INTEGRAL, _Q1_VALOR_M2_INTEGRAL, _Q3_VALOR_M2_INTEGRAL, 
          _NOBS_VALOR_M2_INTEGRAL, IQR_VALOR_M2_INTEGRAL, LI_VALOR_M2_INTEGRAL, LS_VALOR_M2_INTEGRAL, _P1_PRECIO, 
          _P99_PRECIO, _NOBS_PRECIO, _Q1_PRECIO, _Q3_PRECIO, IQR_PRECIO, LI_PRECIO, LS_PRECIO, _P1_AVALUO, _P99_AVALUO, 
          _NOBS_AVALUO, _Q1_AVALUO, _Q3_AVALUO, IQR_AVALUO, LI_AVALUO, LS_AVALUO, OUTLAYER_1, OUTLAYER_3, OUTLAYER_4,
		  CODIGO_BARRIO, CODIGO_MANZANA, CODIGO_PREDIO, CODIGO_CONSTRUCCION, CODIGO_RESTO, DIRECCION_REAL
FROM BASE_HIST_17_21_CONSOLIDADA_19;
QUIT;


DATA RESUMEN; /*9.098*/
SET FOCA.BASE_FOCA_MAY21_OCT21;
/*IF VALIDA_IIC = 'VALIDO   ';*/
IF VALIDA_BARMANPRE = 'VALIDO   ';
IF VALIDA_CRUCE_SIIC = 'VALIDO   ';
IF VALIDA_AREA_CONST= 'VALIDO   ';
IF VALIDA_DIF_AREA_CONST= 'VALIDO   ';
IF VALIDA_DIF_AREA_TERR= 'VALIDO   ';
IF VALIDA_USO = 'VALIDO   ';
IF VALIDA_DES_ECONOMICO= 'VALIDO   ';
IF VALIDA_ZHF= 'VALIDO   ';
IF VALIDA_LOTDISTRIT= 'VALIDO   ';
IF VALIDA_ESTRATO= 'VALIDO   ';
IF MARCA_REPET_FUENTE='NO REPETIDO';
IF MARCA_REPETIDOS='NO REPETIDO';
IF OUTLAYER_1 = '';
IF OUTLAYER_3 = '';
IF OUTLAYER_4 = '';
TIPO_MERCADO = TIPO_OFERTA;
BARRIO = CODIGO_BARRIO;
MANZANA = CODIGO_MANZANA;
PREDIO = CODIGO_PREDIO;
CONSTRUCCION = CODIGO_CONSTRUCCION;
RESTO = CODIGO_RESTO;
DIRECCION = DIRECCION_REAL;
CHIP = '';
MATRICULA = '';
AREA_TERRENO = '';
VALOR = PRECIO;
VALOR_TERRENO= '';
VALOR_CONSTRUCCION= '';
OBSERVACIONES = IDENTIFICADOR;
IF NUMERO_BANOS = . THEN NUMERO_BANOS = 0;
TIPO_DE_INMUEBLE = TIPO_INMUEBLE;
RUN;

PROC SQL; CREATE TABLE FOCA.BASE_FOCA_MAY21_OCT21_FIN
AS SELECT FUENTE, NOMBRE_FUENTE,TIPO_MERCADO, FECHA, DIRECCION, BARRIO, MANZANA, PREDIO, CONSTRUCCION, RESTO, 
          CHIP, MATRICULA, ESTRATO, TIPO_DE_INMUEBLE, AREA_CONSTRUIDA, AREA_TERRENO, VALOR, VALOR_ADMINISTRACION,
          VALOR_TERRENO, VALOR_CONSTRUCCION, NUMERO_ALCOBAS, NUMERO_BA_OS, NUMERO_GARAJES, TIPO_GARAJES, 
          NUMERO_PISOS,VETUSTEZ,OBSERVACIONES
FROM RESUMEN;
QUIT;
