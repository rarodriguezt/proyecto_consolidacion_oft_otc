# Proyecto de consolidación de bases de ofertas de fuentes secundarias

En este README.md se detallan los aspectos más relevantes de este proceso. Se busca documentar este procedimiento.

## Herramientas de georreferenciación

Estas herramientas permiten generar el Código Sector de los inmuebles, particularmente los códigos Barrio, Manzana y Predio (BARMANPRE) asociados a las ofertas analizadas. Dependiendo de la estructura propia con que se presenta la información en cada fuente, resulta más conveniente usar algún método particular sobre otro. A continuación, se presentan las tres herramientas disponibles para realizar la georreferenciación de ofertas y el paso a paso a seguir en cada caso.

1. Traductor (SAS): esta herramienta funciona a partir de la dirección de los inmuebles, y se emplea en las siguientes fuentes: **Metro Cuadrado, Finca Raíz, Galería Inmobiliaria y Cien Cuadras**. 
En la ruta (\\prowin04\Prog_COMUN\ESTADISTICA\source\ADITIONAL_PROCESSES\TRADUCTOR) se encuentran los programas para procesar cada una de las fuentes.
Se requieren permisos otorgados por la Oficina de Sistemas para utilizar estos programas.
Dentro del programa, se debe verificar que el nombre de las bases de datos "DATABASE_IDENTIFICADOR_" y "PREDIO_IDENTIFICADOR_" correspondan al año respectivo de las ofertas.
Se ejecuta la macro "TRADUCE_DIR" definiendo los parámetros correspondientes al año (VIG) y mes (MES) de las ofertas.

2. Arcgis: esta herramienta funciona a partir de las coordenadas los inmuebles, y se emplea en las siguientes fuentes: **Metro Cuadrado, Properati, Finca Raíz y Cien Cuadras**.
En la ruta (\\prowin04\Prog_COMUN\ESTADISTICA\input\z.solicitud_arcgis) se encuentran el código "PROGRAMA_GEO_ARCGIS" de SAS, con el cual se procesan las bases de datos objeto de georreferenciación y se guardan en formato .dbf.
Dentro del programa, se ubica la sección correspondiente a la fuente. Posteriormente, se ejecuta la macro "EXPORT_SAS_FR" definiendo los parámetros correspondientes al año (VIG) y mes (MES) de las ofertas.
En la siguiente ruta (\\prowin04\Prog_COMUN\ESTADISTICA\input\z.solicitud_arcgis\input) se encuentra el archivo exportado, en la carpeta correspondiente al mes y año de las ofertas.
Se remite la solicitud de georreferenciación vía correo electrónico, adjuntando los archivos generados en formato .dbf, a Aureliano del Carmen Amaya Donoso (aamaya@catastrobogota.gov.co).
Una vez se reciba la respuesta con la base georreferenciada, el archivo se debe guardar en formato .txt separado por punto y coma (;) en la ruta (\\prowin04\Prog_COMUN\ESTADISTICA\input\z.solicitud_arcgis\output) dentro de la carpeta correspondiente al mes (o trimestre) y año de las ofertas, garantizando que el nombre siga la siguiente estructura "<año><mes>_<fuente>_ARCGIS".
Finalmente, se utiliza el programa "PROGRAMA_CARGUE_INFO_ARCGIS_<FUENTE>" que se encuentra en la ruta (\\prowin04\Prog_COMUN\ESTADISTICA\source\ADITIONAL_PROCESSES), correspondiente a la fuente respectiva, dentro del cual se debe modificar el nombre del archivo final de acuerdo con las características de las ofertas: "geocodificador_<fuente>_<año>_<mes>_arcgis"

3. Mapas Bogotá: esta herramienta funciona a partir de las coordenadas y la dirección los inmuebles, y se emplea en las siguientes fuentes: **Metro Cuadrado, Properati, Finca Raíz, Galería Inmobiliaria y Cien Cuadras**. 
Para emplear esta herramienta se debe contar con permisos para la opción de "Geocodificar" en el sitio web de Mapas Bogotá (https://mapas.bogota.gov.co/). Previo a la solicitud de permisos, se debe crear una cuenta en el aplicativo con el correo institucional. Una vez creada, se debe realizar la solicitud al correo electrónico ideca@catastrobogota.gov.co.
En la ruta (\\prowin04\Prog_COMUN\ESTADISTICA\input\z.mapas_bogota) se encuentra el código "PROGRAMA_GEO_MAPAS" de R que permite alistar la información para ser cargada a la plataforma de Mapas Bogotá.
En el programa se definen los parámetros iniciales correspondientes al año (anio) mes (mes) y fuente (fuente) correspondiente a las ofertas objeto de georreferenciación.
Se ejecuta todo el script para generar los archivos en formato de excel con la estructura requerida para georreferenciar por el aplicativo.
En la siguiente ruta (\\prowin04\Prog_COMUN\ESTADISTICA\input\z.mapas_bogota\input) se encuentran los archivos generados en formato de Excel listos para cargar en el aplicativo.
Una vez en el aplicativo, en el panel principal se selecciona la opción "Geocodificar".
En el aplicativo se pueden georreferencian las ofertas de dos maneras: 
- Directa: a través de la dirección de la oferta, para identificar el archivo se verifica que su nombre finalice en "dir".
- Inversa: a través de las coordenadas de la oferta, para identificar el archivo se verifica que su nombre finalice en "inv".
En ambos casos, se selecciona el formato "Excel".
Una vez termine el proceso de geocodificación en el aplicativo, la base de datos resultante se recibe en el correo electrónico registrado. El archivo generado se debe guardar en la ruta (\\prowin04\Prog_COMUN\ESTADISTICA\input\z.mapas_bogota\output) y debe seguir la siguiente estructura "<año><mes>_<fuente>_<tipo de georreferenciación>. Por ejemplo, para una base de Finca Raíz de mayo de 2022 geocodificada de manera Directa, el nombre del archivo será "2205_FR_DIR", mientras que en caso de una base de Properati de marzo de 2022 geocodificada de manera Inversa, su nombre será "2203_PR_COOR".
Por último, se debe utilizar el código "PROGRAMA_EXPORTAR_SAS" de SAS, donde se debe modificar la librería (lib) para que quede ajustada de acuerdo con la fuente trabajada.
Finalmente, se ejecuta la macro definiendo los parámetros correspondientes al año (VIG), mes (MES), fuente (FTE) y tipo dependiendo si el método de georreferenciación fue Directa o Inversa (TIPO).

** Los archivos generados al finalizar el uso de cada herramienta son empleados en los programas de procesamiento de información de las ofertas **

## Procesamiento de ofertas

**En todos los casos, cuando se requiera procesar una nueva base de datos esta debe guardarse en la carpeta correspondiente a la fuente y año de las ofertas, en la siguiente ruta (\\prowin04\Prog_COMUN\ESTADISTICA\input\)**
**El nombre con el cual debe guardarse el nuevo archivo debe tener la siguiente estructura "<año><mes>_<fuente>.xlsx", de manera que el nombre del archivo asociado a una base de datos con ofertas de marzo de 2022 de Finca Raíz será "2203_FR.xlsx"**

### Finca Raíz 

1. Para el caso de las ofertas de Finca Raíz, estas se deben georreferenciar por medio de las tres herramientas disponibles: Traductor (dirección), ARCGIS (coordenadas) y Mapas Bogotá (coordenadas y dirección). Para ello, se deben seguir los pasos descritos en las secciones correspondientes a estas herramientas de georreferenciación.


### Properati 

1. Para el caso de las ofertas de Properati, estas se deben georreferenciar por medio de ARCGIS y Mapas Bogotá a través de sus coordenadas. Para ello, se deben seguir los pasos descritos en las secciones correspondientes a estas herramientas de georreferenciación.


### Galería Inmobiliaria

1. En el archivo de excel original, se debe garantizar que las columnas "Fecha Visita", "Fecha Inicio", "Fecha Cierre" y "Fecha Actualización" tengan formato de **Fecha Corta**. Igualmmente, se debe garantizar que el nombre de la hoja donde se encuentra la información sea "Base".
2. Se abre el proyecto de R "ESTADISTICA" que se encuentra en la ruta (\\prowin04\Prog_COMUN\ESTADISTICA). De igual manera, se abre el archivo "220427_PROCESAMIENTO_INFORMACION_GALERIA.R" que se encuentra en la carpeta ADITIONAL_PROCESSES. Se deben ejecutar todas las líneas de código hasta la 31.
3. Se ejecuta la función "GI_titles_dates" estableciendo los parámetros correspondientes a la base de datos analizada.
4. Se valida que se haya creado una copia de la base cuyo nombre finaliza en "_2" (para el caso de la base de mayo de 2022 el nombre de la base es "2205_GI_2.xlsx") y que esta tenga información.
5. Se ejecuta el proceso de georreferenciación a través del TRADUCTOR, empleando el programa "220426_PROGRAMA_TRADUCTOR_GI.SAS" que se encuentra en la ruta (\\prowin04\Prog_COMUN\ESTADISTICA\source\ADITIONAL_PROCESSES\TRADUCTOR). En este programa se debe actualizar la información del año en las bases de PREDIO_IDENTIFICADOR y el mes y año en las macros READ_DATA_GI_XLSX y TRADUCE_DIR.
6. Igualmente, se realiza el proceso de georreferenciación (Directa) a través de Mapas Bogotá empleando el script "PROGRAMA_GEO_MAPAS" de R que se encuentra en la ruta (\\prowin04\Prog_COMUN\ESTADISTICA\input\z.mapas_bogota), en el cual se deben actualizar los parámetros iniciales de "anio", "mes" y "fuente" correspondientes a las bases analizadas.


### Cien Cuadras

1. El archivo original usualmente es compartido en formato *.csv. Este se debe procesar a través del script "IMPORT_CC.R", donde se debe garantizar que las columnas tengan formato adecuado, comparando con las bases de períodos anteriores. Igualmmente, se debe garantizar que el nombre de la hoja sea "CIEN CUADRAS".
2. Se ejecuta el proceso de georreferenciación a través del TRADUCTOR, empleando el programa "220426_PROGRAMA_TRADUCTOR_CC.SAS" que se encuentra en la ruta (\\prowin04\Prog_COMUN\ESTADISTICA\source\ADITIONAL_PROCESSES\TRADUCTOR). En este programa se debe actualizar la información del año en las bases de PREDIO_IDENTIFICADOR y el mes y trimestre en las macros LECTURA_MASIVA_CC y TRADUCE_DIR.
3. Igualmente, se realiza el proceso de georreferenciación (Directa e Indirecta) a través de Mapas Bogotá empleando el script "PROGRAMA_GEO_MAPAS" de R que se encuentra en la ruta (\\prowin04\Prog_COMUN\ESTADISTICA\input\z.mapas_bogota), en el cual se deben actualizar los parámetros iniciales de "anio", "mes" (trimestre) y "fuente" correspondientes a las bases analizadas.


### Metro Cuadrado
